package com.example.bookservice.querry.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.io.Serial;
import java.io.Serializable;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookResponseModel implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @TargetAggregateIdentifier // Dinh nghia id la 1 Identifier
    private String id;
    private String name;
    private String author;
    private Boolean isReady;
}
