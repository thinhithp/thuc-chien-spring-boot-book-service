package com.example.bookservice.querry.projection;

import com.example.bookservice.command.data.Book;
import com.example.bookservice.command.data.BookRepository;
import com.example.bookservice.querry.model.BookResponseModel;
import com.example.bookservice.querry.queries.GetAllBookQuery;
import com.example.bookservice.querry.queries.GetBookQuery;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BookProjection {
    @Autowired
    private BookRepository bookRepository;

    @QueryHandler
    public BookResponseModel handle(GetBookQuery getBookQuery) {
        BookResponseModel model = new BookResponseModel();
        Book book = bookRepository.getById(getBookQuery.getId());
        BeanUtils.copyProperties(book,model);
        return  model;
    }
    @QueryHandler
    public List<BookResponseModel> handle(GetAllBookQuery getAllBookQuery) {
        List<Book> books = bookRepository.findAll();
        List<BookResponseModel> bookResponseModels = new ArrayList<>();
        books.forEach(book -> {
            BookResponseModel model = new BookResponseModel();
            BeanUtils.copyProperties(book,model);
            bookResponseModels.add(model);
        });
        return bookResponseModels;
    }
}
