package com.example.bookservice.querry.controller;

import com.example.bookservice.querry.model.BookResponseModel;
import com.example.bookservice.querry.queries.GetAllBookQuery;
import com.example.bookservice.querry.queries.GetBookQuery;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/books")
public class BookQuerryController {
    @Autowired
    private QueryGateway queryGateway;

    @GetMapping("/{id}")

    public BookResponseModel detail(@PathVariable String id) {
        GetBookQuery getBookQuery = new GetBookQuery();
        getBookQuery.setId(id);
        return queryGateway.query(getBookQuery, ResponseTypes.instanceOf(BookResponseModel.class)).join();
    }
    @GetMapping
    public List<BookResponseModel> list() {
        GetAllBookQuery getAllBookQuery = new GetAllBookQuery();

        return  queryGateway.query(getAllBookQuery, ResponseTypes.multipleInstancesOf(BookResponseModel.class)).join();}
}
