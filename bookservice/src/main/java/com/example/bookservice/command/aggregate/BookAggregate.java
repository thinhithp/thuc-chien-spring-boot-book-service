package com.example.bookservice.command.aggregate;

import com.example.bookservice.command.command.CreateBookCommand;
import com.example.bookservice.command.command.DeleteBookCommand;
import com.example.bookservice.command.command.UpdateBookCommand;
import com.example.bookservice.command.event.BookCreateEvent;
import com.example.bookservice.command.event.BookUpdatedEvent;
import com.example.bookservice.command.event.DeleteBookEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

@Aggregate
public class BookAggregate {
    public BookAggregate() {
    }

    ;
    @AggregateIdentifier // Dinh nghia id la 1 Identifier
    private String id;
    private String name;
    private String author;
    private Boolean isReady;

    @CommandHandler
    public BookAggregate(CreateBookCommand command) {
        BookCreateEvent event = new BookCreateEvent();
        BeanUtils.copyProperties(command, event);
        //
        AggregateLifecycle.apply(event);
    }
    @CommandHandler
    public void handle(UpdateBookCommand updateBookCommand) {
        BookUpdatedEvent event = new BookUpdatedEvent();
        BeanUtils.copyProperties(updateBookCommand, event);
        //
        AggregateLifecycle.apply(event);
    }
    @CommandHandler
    public void handle(DeleteBookCommand deleteBookCommand) {
        DeleteBookEvent event = new DeleteBookEvent();
        BeanUtils.copyProperties(deleteBookCommand, event);
        //
        AggregateLifecycle.apply(event);
    }
    @EventSourcingHandler
    public void on(BookCreateEvent event) {
        this.id = event.getId();
        this.name = event.getName();
        this.author = event.getAuthor();
        this.isReady = event.getIsReady();
    }
    @EventSourcingHandler
    public void on(BookUpdatedEvent event) {
        this.id = event.getId();
        this.name = event.getName();
        this.author = event.getAuthor();
        this.isReady = event.getIsReady();
    }
    @EventSourcingHandler
    public void on(DeleteBookEvent event) {
        this.id = event.getId();

    }

}
