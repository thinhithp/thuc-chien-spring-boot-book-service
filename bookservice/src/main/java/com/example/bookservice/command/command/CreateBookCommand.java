package com.example.bookservice.command.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import javax.persistence.Id;
import java.io.Serial;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateBookCommand implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @TargetAggregateIdentifier // Dinh nghia id la 1 Identifier
    private String id;
    private String name;
    private String author;
    private Boolean isReady;

}
