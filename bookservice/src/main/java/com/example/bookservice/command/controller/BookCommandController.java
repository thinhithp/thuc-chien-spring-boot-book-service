package com.example.bookservice.command.controller;

import com.example.bookservice.command.command.CreateBookCommand;
import com.example.bookservice.command.command.DeleteBookCommand;
import com.example.bookservice.command.command.UpdateBookCommand;
import com.example.bookservice.command.model.BookRequestModel;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/v1/books")
public class BookCommandController {
    @Autowired
    private CommandGateway commandGateway;

    @PostMapping
    public String addBook(@RequestBody BookRequestModel book) {
        CreateBookCommand createBookCommand = new CreateBookCommand(UUID.randomUUID().toString(), book.getName(), book.getAuthor(), true);
       commandGateway.sendAndWait(createBookCommand);
        return "Added Book";
    }
    @PutMapping
    public String updateBook(@RequestBody BookRequestModel book) {
        UpdateBookCommand updateBookCommand = new UpdateBookCommand(book.getId(), book.getName(), book.getAuthor(), book.getIsReady());
       commandGateway.sendAndWait(updateBookCommand);
        return "Updated Book";
    }
    @DeleteMapping("/{id}")
    public String deleteBook(@PathVariable String id) {
        DeleteBookCommand deleteBookCommand = new DeleteBookCommand(id);
       commandGateway.sendAndWait(deleteBookCommand);
        return "Updated Book";
    }
}
