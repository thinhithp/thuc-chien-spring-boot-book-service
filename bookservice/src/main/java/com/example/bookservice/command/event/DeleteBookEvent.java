package com.example.bookservice.command.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serial;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeleteBookEvent implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @Id
    private String id;

}
